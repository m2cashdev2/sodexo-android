package com.m2cash.android.bcode_sender_android.utils

class CannotConnectException(message: String) : Exception(message)
