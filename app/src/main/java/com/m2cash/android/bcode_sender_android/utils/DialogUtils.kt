package com.m2cash.android.bcode_sender_android.utils

import android.content.Context
import android.graphics.Color
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog

class DialogUtils {
    companion object {
        fun showProgressDialog(context: Context, title: String) : SweetAlertDialog {
            val pDialog = SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE)
            pDialog.setTitleText(title)
                    .setCancelable(false)
            pDialog.progressHelper.barColor = Color.parseColor("#A5DC86")
            pDialog.show()
            return pDialog
        }

        fun showAlertDialog(context: Context, title: String, message: String, dialogType: Int) : SweetAlertDialog {
            val aDialog = SweetAlertDialog(context, dialogType)
                    .setTitleText(title)
                    .setContentText(message)
                    .setConfirmText("Ok")
                    .setConfirmClickListener {
                        it.cancel()
                    }
            aDialog.setCancelable(true)
            aDialog.show()
            return aDialog
        }
    }
}