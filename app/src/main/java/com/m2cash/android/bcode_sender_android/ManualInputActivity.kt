package com.m2cash.android.bcode_sender_android

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.m2cash.android.bcode_sender_android.client.BcodeClient
import com.m2cash.android.bcode_sender_android.client.responses.BcodeResponse
import com.m2cash.android.bcode_sender_android.helpers.CustomKeyboard
import com.m2cash.android.bcode_sender_android.utils.CannotConnectException
import com.m2cash.android.bcode_sender_android.utils.DialogUtils
import com.m2cash.android.bcode_sender_android.utils.Utils
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog
import kotlinx.android.synthetic.main.activity_manual_input.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ManualInputActivity : AppCompatActivity() {
    val TAG: String = "ManualInputActivity"

    private val gson = GsonBuilder().setLenient().create()
    private val builder = Retrofit.Builder()
            .baseUrl("http://api.bcodeph.com/")
            .addConverterFactory(GsonConverterFactory.create(gson))
    private val retrofit = builder.build()
    private val client = retrofit.create(BcodeClient::class.java)

    private var pDialog: SweetAlertDialog? = null
    private var message: TextView? = null
    private var title1: TextView? = null
    private var value1: TextView? = null
    private var title2: TextView? = null
    private var value2: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manual_input)
        val mCustomKeyboard = CustomKeyboard(this, R.id.keyboardview, R.xml.hexkbd)
        mCustomKeyboard.registerEditText(R.id.bcode_manual_value)

        submit_manual_btn.setOnClickListener {
            // TODO : SEND BCODE
            try {
                Utils.checkNetworkConnection(applicationContext)

                if (bcode_manual_value.text.isNullOrEmpty()) {
                    Toast.makeText(applicationContext, "Please enter bCode characters.", Toast.LENGTH_SHORT).show()
                } else {
                    pDialog = DialogUtils.showProgressDialog(this, "Processing your request...")
                    checkBcode(bcode_manual_value.text.toString().toUpperCase())
                    Log.e(TAG, bcode_manual_value.text.toString().toUpperCase())
                }
            } catch(e : CannotConnectException){
                Toast.makeText(applicationContext, "Cannot connect to network. Please check you internet connection", Toast.LENGTH_SHORT).show()
            }
        }
        bcode.setOnLongClickListener {
            //            startActivity(Intent(android.provider.Settings.ACTION_INPUT_METHOD_SETTINGS))
            startActivity(Intent(android.provider.Settings.ACTION_SETTINGS))

//            val intent = Intent()
//            intent.action = Intent.ACTION_MAIN
//            intent.addCategory(Intent.CATEGORY_HOME)
//            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//            startActivity(intent)
            true
        }
    }

    fun showBcodeRequestDialog(responseStatus: String?, bcode: String?, sText: String, msg: String?, t1: String?, v1: String?, t2: String?, v2: String?) {
        val alertDialog = AlertDialog.Builder(this@ManualInputActivity).create()
        val inflater = this.layoutInflater
        val dialogInflater = inflater.inflate(R.layout.dialog_response, null)
        val statusValue = dialogInflater.findViewById<ImageView>(R.id.dialog_status_ic)

        val statusText = dialogInflater?.findViewById<TextView>(R.id.status_txt)
        statusText!!.text = sText
        message = dialogInflater?.findViewById<TextView>(R.id.message_txt)
        if (msg == null)
            message!!.visibility = View.GONE
        else {
            message!!.text = msg
            message!!.gravity = Gravity.CENTER
        }

        title1 = dialogInflater.findViewById<TextView>(R.id.title_txt1)
        value1 = dialogInflater.findViewById<TextView>(R.id.value_txt1)
        title2 = dialogInflater.findViewById<TextView>(R.id.title_txt2)
        value2 = dialogInflater.findViewById<TextView>(R.id.value_txt2)

        if (t1 == null)
            title1!!.visibility = View.GONE
        else
            title1!!.text = t1
        if (v1 == null)
            value1!!.visibility = View.GONE
        else
            value1!!.text = v1
        if (t2 == null)
            title2!!.visibility = View.GONE
        else
            title2!!.text = t2
        if (v2 == null)
            value2!!.visibility = View.GONE
        else
            value2!!.text = v2

        alertDialog!!.setView(dialogInflater)

        when (responseStatus) {
            "Claimed" ->{
                statusValue.setImageDrawable(ContextCompat.getDrawable(this@ManualInputActivity, R.drawable.ic_warning))
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { dialog, which ->
                    dialog.dismiss()
                }
            }

            "Valid" -> {
                statusValue.setImageDrawable(ContextCompat.getDrawable(this@ManualInputActivity, R.drawable.ic_check))
                alertDialog.setCancelable(false)
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "CLAIM") { dialog, which ->
                    dialog.dismiss()
                    redeemBcode(bcode!!)
                }
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL") { dialog, which ->
                    dialog.dismiss()
                }
            }
            "Success" ->{
                    statusValue.setImageDrawable(ContextCompat.getDrawable(this@ManualInputActivity, R.drawable.ic_check))
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "CLOSE") { dialog, which ->
                        dialog.dismiss()
                    }
                }

            else ->{
                statusValue.setImageDrawable(ContextCompat.getDrawable(this@ManualInputActivity, R.drawable.ic_error))
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "CLOSE") { dialog, which ->
                    dialog.dismiss()
                }
            }

        }

        alertDialog.show()
        val positiveButton = alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE)
        positiveButton?.textSize = 20.0f
        positiveButton?.setTextColor(Color.BLACK)
        if(responseStatus == "Valid"){
            val negativeButton = alertDialog!!.getButton(AlertDialog.BUTTON_NEGATIVE)
            negativeButton?.textSize = 20.0f
            negativeButton?.setTextColor(Color.BLACK)
        }

        submit_manual_btn.isEnabled = true
        pDialog!!.dismiss()

    }


    fun redeemBcode(bcode: String) {

        val call = client.transactBcode("sodexoapi", "123456", bcode, "SD9999", "1", "1")
        call.enqueue(object : Callback<BcodeResponse> {


            override fun onResponse(call: Call<BcodeResponse>, response: Response<BcodeResponse>) {

                Log.e(TAG, "response: " + response.body()!!)
                if (response.body()!!.status == "Success") {
                    showBcodeRequestDialog("Success", null, "Redemption Successful", "Thank you!", null, null, null, null)

                } else if (response.body()!!.status == "Claimed") {
                    showBcodeRequestDialog("Claimed", null, "Claimed", null, "Claimed Date: ", response.body()!!.redemptiontimestamp, "Store Name: ", response.body()!!.branch)

                }
//                bcode_manual_value.setText("")
            }

            override fun onFailure(call: Call<BcodeResponse>, t: Throwable) {
                Log.e(TAG, "failed: " + t.message)
                showBcodeRequestDialog(null, null, "Invalid", "Unknown Code", null, null, null, null)
//                bcode_manual_value.setText("")
            }
        })

    }

    fun checkBcode(bcode: String) {
        submit_manual_btn.isEnabled = false
        val call = client.transactBcode("sodexoapi", "123456", bcode, "SD9999", "1", "0")
        call.enqueue(object : Callback<BcodeResponse> {

            override fun onResponse(call: Call<BcodeResponse>, response: Response<BcodeResponse>) {

                Log.e(TAG, "response: " + response.body()!!)
                if (response.body()!!.status == "Valid") {
                    var promoCode : String? = null
                    if(response.body()!!.promotionitemcode == "G10")
                        promoCode = "G10 - SDX500"
                    else if(response.body()!!.promotionitemcode == "G11")
                        promoCode = "G11 - SDX1000"

                    if (response.body()!!.ref.equals(bcode_manual_value.text.toString())) {
                        showBcodeRequestDialog("Valid", bcode, "Valid for Redemption", response.body()!!.notes, "bCode: ", response.body()!!.bcode, "Promo Code: ", promoCode)

                    }
                } else if (response.body()!!.status.equals("Claimed")) {
                    if (response.body()!!.ref == bcode_manual_value.text.toString()) {
                        showBcodeRequestDialog("Claimed", null, "Claimed", null, "Claimed Date: ", response.body()!!.redemptiontimestamp, "Store Name: ", response.body()!!.branch)
                    }
                }
            }

            override fun onFailure(call: Call<BcodeResponse>, t: Throwable) {
                Log.e(TAG, "failed: " + t.message)
                showBcodeRequestDialog(null, null, "Invalid", "Unknown Code", null, null, null, null)
                pDialog!!.dismiss()
            }
        })

    }

}
