package com.m2cash.android.bcode_sender_android

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PixelFormat
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.m2cash.android.bcode_sender_android.client.BcodeClient
import com.m2cash.android.bcode_sender_android.client.responses.BcodeResponse
import com.m2cash.android.bcode_sender_android.helpers.CustomKeyboard
import com.m2cash.android.bcode_sender_android.helpers.CustomViewGroup
import com.m2cash.android.bcode_sender_android.utils.CannotConnectException
import com.m2cash.android.bcode_sender_android.utils.DialogUtils
import com.m2cash.android.bcode_sender_android.utils.Utils
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    val TAG: String = "MainActivity"
    private var bCodeScanStr: String = ""

    private val gson = GsonBuilder().setLenient().create()
    private val builder = Retrofit.Builder()
            .baseUrl("http://api.bcodeph.com/")
            .addConverterFactory(GsonConverterFactory.create(gson))
    private val retrofit = builder.build()
    private val client = retrofit.create(BcodeClient::class.java)

    private var pDialog: SweetAlertDialog? = null
    private var message: TextView? = null
    private var title1: TextView? = null
    private var value1: TextView? = null
    private var title2: TextView? = null
    private var value2: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        switch_manual_btn.setOnClickListener {
            startActivity(Intent(this, ManualInputActivity::class.java))
        }
//        disableStatusBar()
        sodexologo.setOnLongClickListener {
            startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
            true
        }

    }


    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        try {
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                Utils.checkNetworkConnection(applicationContext)

                pDialog = DialogUtils.showProgressDialog(this, "Processing your request...")
                checkBcode(bCodeScanStr)
//            Toast.makeText(this, bCodeScanStr, Toast.LENGTH_SHORT).show()
                bCodeScanStr = ""
            } else {
                bCodeScanStr += event?.unicodeChar?.toChar().toString()
            }

        } catch (e: CannotConnectException){
            Toast.makeText(applicationContext, "Cannot connect to network. Please check you internet connection", Toast.LENGTH_SHORT).show()
        }

        return true
    }


    fun disableStatusBar() {

        val manager = applicationContext
                .getSystemService(Context.WINDOW_SERVICE) as WindowManager

        val localLayoutParams = WindowManager.LayoutParams()
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR
        localLayoutParams.gravity = Gravity.TOP
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or

                // this is to enable the notification to receive touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or

                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        localLayoutParams.height = (50 * resources
                .displayMetrics.scaledDensity).toInt()
        localLayoutParams.format = PixelFormat.TRANSPARENT

        val view = CustomViewGroup(this)

        manager.addView(view, localLayoutParams)
    }

    override fun onResume() {
        super.onResume()
        val uriPath = "android.resource://" + packageName + "/" + R.raw.help
        val uri = android.net.Uri.parse(uriPath)
        bcode_tutorial_vv.setVideoURI(uri)
        bcode_tutorial_vv.start()
        bcode_tutorial_vv.setOnPreparedListener { mp ->
            mp.isLooping = true
            mp.setScreenOnWhilePlaying(false)
        }
    }

    override fun onBackPressed() {
    }

    fun showBcodeRequestDialog(responseStatus: String?, bcode: String?, sText: String, msg: String?, t1: String?, v1: String?, t2: String?, v2: String?) {
        val alertDialog = AlertDialog.Builder(this@MainActivity).create()
        val inflater = this.layoutInflater
        val dialogInflater = inflater!!.inflate(R.layout.dialog_response, null)
        val statusValue = dialogInflater!!.findViewById<ImageView>(R.id.dialog_status_ic)

        val statusText = dialogInflater?.findViewById<TextView>(R.id.status_txt)
        statusText!!.text = sText
        message = dialogInflater?.findViewById<TextView>(R.id.message_txt)
        if (msg == null)
            message!!.visibility = View.GONE
        else {
            message!!.text = msg
            message!!.gravity = Gravity.CENTER
        }

        title1 = dialogInflater?.findViewById<TextView>(R.id.title_txt1)
        value1 = dialogInflater?.findViewById<TextView>(R.id.value_txt1)
        title2 = dialogInflater?.findViewById<TextView>(R.id.title_txt2)
        value2 = dialogInflater?.findViewById<TextView>(R.id.value_txt2)

        if (t1 == null)
            title1!!.visibility = View.GONE
        else
            title1!!.text = t1
        if (v1 == null)
            value1!!.visibility = View.GONE
        else
            value1!!.text = v1
        if (t2 == null)
            title2!!.visibility = View.GONE
        else
            title2!!.text = t2
        if (v2 == null)
            value2!!.visibility = View.GONE
        else
            value2!!.text = v2

        alertDialog!!.setView(dialogInflater)

        when (responseStatus) {
            "Claimed" -> {
                statusValue!!.setImageDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_warning))
                alertDialog!!.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { dialog, which ->
                    dialog.dismiss()
                }
            }

            "Valid" -> {
                statusValue!!.setImageDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_check))
                alertDialog.setCancelable(false)
                alertDialog!!.setButton(AlertDialog.BUTTON_POSITIVE, "CLAIM") { dialog, which ->
                    dialog.dismiss()
                    redeemBcode(bcode!!)
                }
                alertDialog!!.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL") { dialog, which ->
                    dialog.dismiss()
                }
            }
            "Success" -> {
                statusValue!!.setImageDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_check))
                alertDialog!!.setButton(AlertDialog.BUTTON_POSITIVE, "CLOSE") { dialog, which ->
                    dialog.dismiss()
                }
            }

            else -> {
                statusValue!!.setImageDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_error))
                alertDialog!!.setButton(AlertDialog.BUTTON_POSITIVE, "CLOSE") { dialog, which ->
                    dialog.dismiss()
                }
            }

        }

        alertDialog!!.show()
        val positiveButton = alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE)
        positiveButton?.textSize = 20.0f
        positiveButton?.setTextColor(Color.BLACK)
        if (responseStatus == "Valid") {
            val negativeButton = alertDialog!!.getButton(AlertDialog.BUTTON_NEGATIVE)
            negativeButton?.textSize = 20.0f
            negativeButton?.setTextColor(Color.BLACK)
        }

        pDialog!!.dismiss()

    }


    fun redeemBcode(bcode: String) {

        val call = client.transactBcode("sodexoapi", "123456", bcode, "SD9999", "1", "1")
        call.enqueue(object : Callback<BcodeResponse> {


            override fun onResponse(call: Call<BcodeResponse>, response: Response<BcodeResponse>) {

                Log.e(TAG, "response: " + response.body()!!)
                if (response.body()!!.status == "Success") {
                    showBcodeRequestDialog("Success", null, "Redemption Successful", "Thank you!", null, null, null, null)

                } else if (response.body()!!.status == "Claimed") {
                    showBcodeRequestDialog("Claimed", null, "Claimed", null, "Claimed Date: ", response.body()!!.redemptiontimestamp, "Store Name: ", response.body()!!.branch)

                }
//                bcode_manual_value.setText("")
            }

            override fun onFailure(call: Call<BcodeResponse>, t: Throwable) {
                Log.e(TAG, "failed: " + t.message)
                showBcodeRequestDialog(null, null, "Invalid", "Unknown Code", null, null, null, null)
//                bcode_manual_value.setText("")
            }
        })

    }

    fun checkBcode(bcode: String) {
        val call = client.transactBcode("sodexoapi", "123456", bcode, "SD9999", "1", "0")
        call.enqueue(object : Callback<BcodeResponse> {

            override fun onResponse(call: Call<BcodeResponse>, response: Response<BcodeResponse>) {

                Log.e(TAG, "response: " + response.body()!!)
                if (response.body()!!.status == "Valid") {
                    var promoCode: String? = null
                    if (response.body()!!.promotionitemcode == "G10")
                        promoCode = "G10 - SDX500"
                    else if (response.body()!!.promotionitemcode == "G11")
                        promoCode = "G11 - SDX1000"

                    showBcodeRequestDialog("Valid", bcode, "Valid for Redemption", response.body()!!.notes, "bCode: ", response.body()!!.bcode, "Promo Code: ", promoCode)

                } else if (response.body()!!.status.equals("Claimed")) {
                    showBcodeRequestDialog("Claimed", null, "Claimed", null, "Claimed Date: ", response.body()!!.redemptiontimestamp, "Store Name: ", response.body()!!.branch)

                }
            }

            override fun onFailure(call: Call<BcodeResponse>, t: Throwable) {
                Log.e(TAG, "failed: " + t.message)
                showBcodeRequestDialog(null, null, "Invalid", "Unknown Code", null, null, null, null)
                pDialog!!.dismiss()
            }
        })

    }

//    fun redeemBcode(bcode: String) {
//        val alertDialog = AlertDialog.Builder(this@MainActivity).create()
//        val inflater = this.getLayoutInflater()
//        val gson = GsonBuilder().setLenient().create()
//        val builder = Retrofit.Builder()
//                .baseUrl("http://api-test.bcodeph.com/")
//                .addConverterFactory(GsonConverterFactory.create(gson))
//        val retrofit = builder.build()
//        val pDialog = DialogUtils.showProgressDialog(this, "Processing your request...")
//        val client = retrofit.create(BcodeClient::class.java)
//        val call = client.transactBcode("sodexoapi", "123456", bcode, "SD999", "1", "1")
//        call.enqueue(object : Callback<BcodeResponse> {
//
//
//            override fun onResponse(call: Call<BcodeResponse>, response: Response<BcodeResponse>) {
//
//                Log.e(TAG, "response: " + response.body()!!)
//                if (response.body()!!.status.equals("Success")) {
//                    val dialogInflater = inflater.inflate(R.layout.dialog_response, null)
//                    val statusValue = dialogInflater.findViewById<ImageView>(R.id.dialog_status_ic)
//                    statusValue.setImageDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_check))
//                    val statusText = dialogInflater.findViewById<TextView>(R.id.status_txt)
//                    statusText.setText("Successful redemption")
//                    val message= dialogInflater.findViewById<TextView>(R.id.message_txt)
//                    message.setText("Thank you")
//                    val title1= dialogInflater.findViewById<TextView>(R.id.title_txt1)
//                    val value1= dialogInflater.findViewById<TextView>(R.id.value_txt1)
//                    val title2= dialogInflater.findViewById<TextView>(R.id.title_txt2)
//                    val value2= dialogInflater.findViewById<TextView>(R.id.value_txt2)
//                    title1.visibility = View.GONE
//                    value1.visibility = View.GONE
//                    title2.visibility = View.GONE
//                    value2.visibility = View.GONE
//                    alertDialog.setView(dialogInflater)
//                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "CLOSE") { dialog, which ->
//                        dialog.dismiss()
//                        pDialog.dismiss()
//                    }
//                    alertDialog.show()
//                    val positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE) as Button?
//                    positiveButton?.textSize = 20.0f
//                    positiveButton?.setTextColor(Color.BLACK)
//
//
//                } else if (response.body()!!.status.equals("Claimed")) {
//                    val dialogInflater = inflater.inflate(R.layout.dialog_response, null)
//                    val statusValue = dialogInflater.findViewById<ImageView>(R.id.dialog_status_ic)
//                    statusValue.setImageDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_warning))
//                    val statusText = dialogInflater.findViewById<TextView>(R.id.status_txt)
//                    statusText.setText("Claimed")
//                    val message= dialogInflater.findViewById<TextView>(R.id.message_txt)
//                    message.visibility = View.GONE
//                    val title1= dialogInflater.findViewById<TextView>(R.id.title_txt1)
//                    val value1= dialogInflater.findViewById<TextView>(R.id.value_txt1)
//                    title1.setText("Claimed Date: ")
//                    value1.setText(response.body()!!.bcode)
//                    val title2= dialogInflater.findViewById<TextView>(R.id.title_txt2)
//                    val value2= dialogInflater.findViewById<TextView>(R.id.value_txt2)
//                    title2.setText("Store Name: ")
//                    value2.setText(response.body()!!.branch)
//                    alertDialog.setView(dialogInflater)
//                    alertDialog.setView(dialogInflater)
//                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { dialog, which ->
//                        dialog.dismiss()
//                        pDialog.dismiss()
//                    }
//                    alertDialog.show()
//                    val positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE) as Button?
//                    positiveButton?.textSize = 20.0f
//                    positiveButton?.setTextColor(Color.BLACK)
//
//                }
//            }
//
//            override fun onFailure(call: Call<BcodeResponse>, t: Throwable) {
//                Log.e(TAG, "failed: " + t.message)
//                pDialog.dismiss()
//            }
//        })
//
//    }
//
//    fun checkBcode(bcode: String) {
//        val gson = GsonBuilder().setLenient().create()
//        val builder = Retrofit.Builder()
//                .baseUrl("http://api-test.bcodeph.com/")
//                .addConverterFactory(GsonConverterFactory.create(gson))
//        val retrofit = builder.build()
//        val alertDialog = AlertDialog.Builder(this@MainActivity).create()
//        val inflater = this.getLayoutInflater()
//        val pDialog = DialogUtils.showProgressDialog(this, "Processing your request...")
//        val client = retrofit.create(BcodeClient::class.java)
//        val call = client.transactBcode("sodexoapi", "123456", bcode, "SD999", "1", "0")
//        call.enqueue(object : Callback<BcodeResponse> {
//
//
//            override fun onResponse(call: Call<BcodeResponse>, response: Response<BcodeResponse>) {
//
//                Log.e(TAG, "response: " + response.body()!!)
//                if (response.body()!!.status.equals("Valid")) {
//                    val dialogInflater = inflater.inflate(R.layout.dialog_response, null)
//                    val statusValue = dialogInflater.findViewById<ImageView>(R.id.dialog_status_ic)
//                    statusValue.setImageDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_check))
//                    val statusText = dialogInflater.findViewById<TextView>(R.id.status_txt)
//                    statusText.setText("Valid for Redemption")
//                    val message= dialogInflater.findViewById<TextView>(R.id.message_txt)
//                    message.setText(response.body()!!.notes)
//                    val title1= dialogInflater.findViewById<TextView>(R.id.title_txt1)
//                    val value1= dialogInflater.findViewById<TextView>(R.id.value_txt1)
//                    title1.setText("bCode: ")
//                    value1.setText(response.body()!!.bcode)
//                    val title2= dialogInflater.findViewById<TextView>(R.id.title_txt2)
//                    val value2= dialogInflater.findViewById<TextView>(R.id.value_txt2)
//                    title2.setText("Promo code: ")
//                    value2.setText(response.body()!!.promotionitemcode)
//                    alertDialog.setView(dialogInflater)
//                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "CLAIM") { dialog, which ->
//                        redeemBcode(bcode)
//                        pDialog.dismiss()
//                    }
//                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL") { dialog, which ->
//                        dialog.dismiss()
//                        pDialog.dismiss()
//                    }
//                    alertDialog.show()
//                    val positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE) as Button?
//                    val negativeButton = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE) as Button?
//                    positiveButton?.textSize = 20.0f
//                    positiveButton?.setTextColor(Color.BLACK)
//                    negativeButton?.textSize = 20.0f
//                    negativeButton?.setTextColor(Color.BLACK)
//
//
//                } else if (response.body()!!.status.equals("Claimed")) {
//
//                    val dialogInflater = inflater.inflate(R.layout.dialog_response, null)
//                    val statusValue = dialogInflater.findViewById<ImageView>(R.id.dialog_status_ic)
//                    statusValue.setImageDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_warning))
//                    val statusText = dialogInflater.findViewById<TextView>(R.id.status_txt)
//                    statusText.setText("Claimed")
//                    val message= dialogInflater.findViewById<TextView>(R.id.message_txt)
//                    message.visibility = View.GONE
//                    val title1= dialogInflater.findViewById<TextView>(R.id.title_txt1)
//                    val value1= dialogInflater.findViewById<TextView>(R.id.value_txt1)
//                    title1.setText("Claimed Date: ")
//                    value1.setText(response.body()!!.bcode)
//                    val title2= dialogInflater.findViewById<TextView>(R.id.title_txt2)
//                    val value2= dialogInflater.findViewById<TextView>(R.id.value_txt2)
//                    title2.setText("Store Name: ")
//                    value2.setText(response.body()!!.branch)
//                    alertDialog.setView(dialogInflater)
//                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "CLOSE") { dialog, which ->
//                        dialog.dismiss()
//                        pDialog.dismiss()
//                    }
//                    alertDialog.show()
//                    val positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE) as Button?
//                    positiveButton?.textSize = 20.0f
//                    positiveButton?.setTextColor(Color.BLACK)
//                }
//            }
//
//            override fun onFailure(call: Call<BcodeResponse>, t: Throwable) {
//                Log.e(TAG, "failed: " + t.message)
//                val dialogInflater = inflater.inflate(R.layout.dialog_response, null)
//                val statusValue = dialogInflater.findViewById<ImageView>(R.id.dialog_status_ic)
//                statusValue.setImageDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_error))
//                val statusText = dialogInflater.findViewById<TextView>(R.id.status_txt)
//                statusText.setText("Invalid")
//                val message= dialogInflater.findViewById<TextView>(R.id.message_txt)
//                message.setText("Unknown Code")
//                val title1= dialogInflater.findViewById<TextView>(R.id.title_txt1)
//                val value1= dialogInflater.findViewById<TextView>(R.id.value_txt1)
//                val title2= dialogInflater.findViewById<TextView>(R.id.title_txt2)
//                val value2= dialogInflater.findViewById<TextView>(R.id.value_txt2)
//                title1.visibility = View.GONE
//                value1.visibility = View.GONE
//                title2.visibility = View.GONE
//                value2.visibility = View.GONE
//                alertDialog.setView(dialogInflater)
//                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "CLOSE") { dialog, which ->
//                    dialog.dismiss()
//                    pDialog.dismiss()
//                }
//                alertDialog.show()
//                val positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE) as Button?
//                positiveButton?.textSize = 20.0f
//                positiveButton?.setTextColor(Color.BLACK)
//            }
//        })
//
//    }
}
