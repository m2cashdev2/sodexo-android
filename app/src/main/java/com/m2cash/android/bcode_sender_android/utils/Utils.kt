package com.m2cash.android.bcode_sender_android.utils

import android.content.Context
import android.net.ConnectivityManager
import com.m2cash.android.bcode_sender_android.R


object Utils {

    fun isNetworkAvailable(context: Context): Boolean {
        val ctx = context.applicationContext ?: return false
        val connectivityManager = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    @Throws(CannotConnectException::class)
    fun checkNetworkConnection(context: android.content.Context) {
        if (!com.m2cash.android.bcode_sender_android.utils.Utils.isNetworkAvailable(context))
            throw CannotConnectException(context.getString(R.string.no_internet_connection))
    }
}
