package com.m2cash.android.bcode_sender_android.client

import com.m2cash.android.bcode_sender_android.client.responses.BcodeResponse
import retrofit2.Call;
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface BcodeClient {

    @GET("do2?")
    fun transactBcode(@Query("u") username: String, @Query("p") password: String, @Query("BCODEref") bcodeRef: String, @Query("branch_id") branchId: String, @Query("format") format: String,
                      @Query("mode") mode: String): Call<BcodeResponse>
}