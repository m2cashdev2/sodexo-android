package com.m2cash.android.bcode_sender_android.client.responses

data class BcodeResponse(var status: String, var bcode: String, var ref: String, var promotioncode: String, var promotionitemcode: String, var notes: String
                         , var transactionid: String, var branch: String, var ipaddress: String, var redemptiontimestamp: String, var transactioncode: String)

/*{"status bcode ref promotioncode promotionitemcode notes transactionid branch ipaddress redemptiontimestamp transactioncode*/
